/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.arraydeletionslab;

public class ArrayDeletionsLab {
    
    static int [] deleteElementByIndex(int[] arr, int index){
        int [] newArray = new int[arr.length-1];
        for(int i=0; i<newArray.length; i++) {
            if(i >= index) {
                newArray[i] = arr[i+1];
            }else{
                newArray[i] = arr[i];
            }
        }
        return newArray;
    }
    
    static int [] deleteElementByValue(int[] arr, int value){
        int [] newArray = new int[arr.length-1];
        int index = 0;
        for(int i=0; i<newArray.length; i++){
            if(arr[i] == value){
                index = i;
                if(i >= index){
                    newArray[i] = arr[i+1];
                }
            }else{
                newArray[i] = arr[i];
            }
        }return newArray;
    }

    public static void main(String[] args) {
        int [] arr1 = {1,2,3,4,5};
        int [] returnarr = deleteElementByIndex(arr1, 2);
        for(int i=0; i<returnarr.length; i++){
            System.out.print(returnarr[i] + " ");
        }System.out.println();
        
        int [] returnarr2 = deleteElementByValue(returnarr, 4);
        for(int i=0; i<returnarr2.length; i++){
            System.out.print(returnarr2[i] + " ");
        }

    }
}
